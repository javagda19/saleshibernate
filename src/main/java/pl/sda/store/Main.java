package pl.sda.store;

import pl.sda.store.db.EntityDao;
import pl.sda.store.db.InventoryManager;
import pl.sda.store.db.InvoiceManager;
import pl.sda.store.db.ProductManager;
import pl.sda.store.model.Inventory;
import pl.sda.store.model.Invoice;
import pl.sda.store.model.Product;
import pl.sda.store.model.ProductSale;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        EntityDao entityDao = new EntityDao();
        InventoryManager inventoryManager = new InventoryManager();
        ProductManager productManager = new ProductManager();
        InvoiceManager invoiceManager = new InvoiceManager();

        String command;

        do {
            System.out.println("Podaj komendę:");
            command = scanner.next();

            if (command.equalsIgnoreCase("add_product")) {
                System.out.println("Podaj nazwę produktu:");
                String name = scanner.next();

                productManager.saveProduct(name);
            } else if (command.equalsIgnoreCase("add_to_inventory")) {
                System.out.println("Podaj ilosc:");
                Double quantity = scanner.nextDouble();

                System.out.println("Podaj wartość");
                Double value = scanner.nextDouble();

                System.out.println("Podaj identyfikator produktu:");
                String identifier = scanner.next();
                Long identifierProductLong = Long.parseLong(identifier);

                inventoryManager.addToInventory(identifierProductLong, new Inventory(quantity, value));
            } else if (command.equalsIgnoreCase("list_products")) {
                System.out.println("Produkty: ");
                productManager.listAllProducts();
            } else if (command.equalsIgnoreCase("list_inventory")) {
                System.out.println("Inventory:");
                System.out.println("Podaj id produktu:");
                Long identifier = scanner.nextLong();

                productManager.listInventory(identifier);
            } else if (command.equalsIgnoreCase("list_full_inventory")) {
                inventoryManager.listFullInventory();
            } else if (command.equalsIgnoreCase("add_invoice")) {
                invoiceManager.addInvoice(new Invoice());
            } else if (command.equalsIgnoreCase("add_sale")) {
                System.out.println("Podaj id fakury:");
                Long id = scanner.nextLong();

                System.out.println("Podaj ilość:");
                Double quantity = scanner.nextDouble();

                System.out.println("Podaj cene:");
                Double price = scanner.nextDouble();

                String restOfLine = scanner.nextLine().trim();
                String[] params = restOfLine.split(" ");

                List<Long> productIds = new ArrayList<>();
                for (int i = 0; i < params.length; i++) {
                    System.out.println("Podaj identyfikator produktu:");
                    Long productId = Long.parseLong(params[i]);

                    productIds.add(productId);
                }
                invoiceManager.addSale(id, new ProductSale(quantity, price), productIds);
            } else if (command.equalsIgnoreCase("print_invoice")) {
                System.out.println("Podaj identyfikator faktury:");
                Long id = scanner.nextLong();

                invoiceManager.printAllAboutInvoice(id);
            } else if (command.equalsIgnoreCase("print_invoices")) {
                invoiceManager.printInvoices();
            }
        } while (!command.equalsIgnoreCase("stop"));
    }
}

package pl.sda.store.model;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Inventory implements IBaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Double quantity;
    private Double value;

    @CreationTimestamp
    private LocalDate dateArrived;

    public Inventory(Double quantity, Double value) {
        this.quantity = quantity;
        this.value = value;
//        this.dateArrived = LocalDate.now();
    }

    @ManyToOne
    private Product product;

}

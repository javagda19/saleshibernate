package pl.sda.store.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;


@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Invoice implements IBaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @CreationTimestamp
    private LocalDate saleDate;

    @UpdateTimestamp
    private LocalDate modificationDate;

    @OneToMany(fetch = FetchType.EAGER)
//    @JoinColumn(referencedColumnName = "id")
    private Set<ProductSale> productSales;
}

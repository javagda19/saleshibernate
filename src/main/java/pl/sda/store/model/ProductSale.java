package pl.sda.store.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.AUTO;


@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
public class ProductSale implements IBaseEntity{
    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;

    private Double quantity;
    private Double price;

    @ManyToMany(fetch = FetchType.EAGER)
    @ToString.Exclude
    private Set<Product> productSet = new HashSet<>();

    public ProductSale(Double quantity, Double price) {
        this.quantity = quantity;
        this.price = price;
    }
//    @ManyToOne()
//    private Invoice invoice;
}

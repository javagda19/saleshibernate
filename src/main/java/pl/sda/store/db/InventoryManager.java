package pl.sda.store.db;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.TransactionException;
import pl.sda.store.model.Inventory;
import pl.sda.store.model.Invoice;
import pl.sda.store.model.Product;
import pl.sda.store.model.ProductSale;

import javax.persistence.PersistenceException;
import java.util.List;
import java.util.Optional;

public class InventoryManager {
    private static final EntityDao entityDao = new EntityDao();

    public void addToInventory(Long productId, Inventory inventory) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            Product product = session.get(Product.class, productId);
            product.getInventories().add(inventory);

            inventory.setProduct(product);

            session.saveOrUpdate(product); // save or update
            session.saveOrUpdate(inventory); // save or update

            transaction.commit();
        } catch (PersistenceException te) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    public void listFullInventory() {
        List<Inventory> inventories = entityDao.getListOfAll(Inventory.class);
        System.out.println("All inventories:");
        for (Inventory i : inventories) {
            System.out.println(i.getId() + ". " + i);
        }
    }
}

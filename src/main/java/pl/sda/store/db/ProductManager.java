package pl.sda.store.db;

import pl.sda.store.model.Inventory;
import pl.sda.store.model.Product;

import java.util.List;
import java.util.Optional;

public class ProductManager {
    private static final EntityDao entityDao = new EntityDao();

    public void saveProduct(String name) {
        Product product = new Product();
        product.setName(name);

        entityDao.save(product);
    }

    public void listAllProducts() {
        List<Product> productList = entityDao.getListOfAll(Product.class);
        for (Product p : productList) {
            System.out.println(p.getId() + ". " + p);
        }
    }

    public void listInventory(Long productId){
        Optional<Product> productOpt = entityDao.getById(Product.class, productId);
        if(productOpt.isPresent()){
            System.out.println("Znaleziono, inventory:");
            Product product = productOpt.get();
            for (Inventory i : product.getInventories()) {
                System.out.println(i.getId() + ". " + i);
            }
        }else{
            System.out.println("Produkt nie został znaleziony.");
        }
    }
}


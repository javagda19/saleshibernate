package pl.sda.store.db;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.store.model.Invoice;
import pl.sda.store.model.Product;
import pl.sda.store.model.ProductSale;

import javax.persistence.PersistenceException;
import java.util.List;
import java.util.Optional;

public class InvoiceManager {
    private final static EntityDao entityDao = new EntityDao();

    public void addInvoice(Invoice invoice) {
        entityDao.save(invoice);

        System.out.println("Stworzono fakturę o identyfikatorze: " + invoice.getId());
    }

    public void addSale(Long id, ProductSale productSale, List<Long> productIds) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            Invoice invoice = session.get(Invoice.class, id);
            invoice.getProductSales().add(productSale);

            session.saveOrUpdate(invoice); // save or update

            for (Long productId : productIds) {
                Product product = session.get(Product.class, productId);
                product.getSales().add(productSale);

                productSale.getProductSet().add(product);
                session.saveOrUpdate(product);
            }

            session.saveOrUpdate(productSale); // save or update

            transaction.commit();
        } catch (PersistenceException te) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
        System.out.println("Dodaję sprzedaż do faktury o identyfikatorze: " + id + ". Sprzedaż " + productSale.getQuantity() + " produktów o cenie " + productSale.getPrice() + " zł.");
    }

    /**
     * Faktura dodana dnia: 2019-04-14, ostatnio zmodyfikowana: 2019-04-14
     * Sprzedane produkty:
     * - 35 szt. produktu o wartości 4.2
     * - 100 szt. produktu o wartości 0.5
     * <p>
     * Suma faktury: 197 zł.
     *
     * @param id
     */
    public void printAllAboutInvoice(Long id) {
        Optional<Invoice> invoiceOpt = entityDao.getById(Invoice.class, id);
        if (invoiceOpt.isPresent()) {
            Invoice invoice = invoiceOpt.get();

            System.out.println("Faktura dodana dnia: " + invoice.getSaleDate() + ", ostatnio zmodyfikowana: " + invoice.getModificationDate());
            System.out.println("Sprzedane produkty:");
            for (ProductSale productSale : invoice.getProductSales()) {
                for (Product prod : productSale.getProductSet()) {
                    System.out.println("- " + productSale.getQuantity() + " szt. produktu (" + prod.getName() + ", id: " + prod.getId() + ") o wartości: " + productSale.getPrice());
                }
            }

            Double sum = invoice.getProductSales()
                    .stream()
                    .mapToDouble(sale -> sale.getPrice() * sale.getQuantity())
                    .sum();

            System.out.println("Suma faktury: " + sum + " zł.");
        } else {
            System.out.println("Nie odnaleziono faktury.");
        }
    }

    public void printInvoices() {
        List<Invoice> invoices = entityDao.getListOfAll(Invoice.class);

        int i = 1;
        for (Invoice inv : invoices) {
            System.out.println(i + ". " + inv.getId() + " data wydania: " + inv.getSaleDate());
        }
    }
}
